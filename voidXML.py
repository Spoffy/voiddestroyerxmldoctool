import xml.etree.ElementTree as ElementTree
import collections

_xmlEndSymbol = "?>"
_xmlEndOffset = 3

def parseFile(filePath):
	"""Intended for VoidDestroyer's XML, which has no single root node.
	This implements a fix/workaround.
	"""
	fileContents = open(filePath).read()
	xmlDeclarationEnd = fileContents.find(_xmlEndSymbol) + _xmlEndOffset
	fileContents = fileContents[0:xmlDeclarationEnd] + "<root>" + fileContents[xmlDeclarationEnd:-1] + "</root>"
	rootNode = ElementTree.fromstring(fileContents)
	return rootNode

class component(object):
	def __init__(self):
		self.attributes = set()
		self.possibleContents = set()

class voidEntityDoc(object):
	name = ""
	components = collections.defaultdict(component)

	def updateFromXmlElement(self, node):
		thisComponent = self.components[node.tag]
		for key in node.attrib.keys():
			thisComponent.attributes.add(key)
		for child in node:
			thisComponent.possibleContents.add(child.tag)
			self.updateFromXmlElement(child)

def xmlElementToVoidDoc(eTreeRoot):
	doc = voidEntityDoc()
	doc.updateFromXmlElement(eTreeRoot)
	return doc
	

import voidXML

root = voidXML.parseFile("sample.xml")
doc = voidXML.xmlElementToVoidDoc(root)

#Temp test. Remove when done.
#System relies in unique element names. Maybe change to consider it as a full tree/DOM again, similar to the one produced by ElementTree.
def recurse(level, name, component, output):
	if level > 10:
		return output
	for i in range(level):
		output += "\t"
	output += "<" + name
	for attribute in component.attributes:
		output += " " + attribute + "="
	output += ">\n"
	for tag in component.possibleContents:
		output = recurse(level+1, tag, doc.components[tag], output)
	return output

print(recurse(0, "root", doc.components["root"], ""))


	
